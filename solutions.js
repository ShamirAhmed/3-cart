const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        }, {
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]


function priceMoreThen65(products) {
    const newProduct = Object.entries(products[0]);
    let newUtensils;
    const items = newProduct.filter((product) => {
        if (product[0] === 'utensils') {
            const utensils = product[1].filter((inside) => {
                const insideItem = Object.entries(inside);
                const returnProduct = insideItem[0][1].price
                    .replace('$', '') > 65;
                return returnProduct;
            });
            newUtensils = utensils;
        } else {
            return product[1].price
                .replace('$', '') > 65;
        }
    })

    const newItems = Object.fromEntries(items);

    return newUtensils.concat(newItems);
}

function quantityMoreThen1(products) {
    const newProduct = Object.entries(products[0]);
    let newUtensils;
    const items = newProduct.filter((product) => {
        if (product[0] === 'utensils') {
            const utensils = product[1].filter((inside) => {
                const insideItem = Object.entries(inside);
                return insideItem[0][1].quantity > 1;
            });
            newUtensils = utensils;
        } else {
            return product[1].quantity > 1;
        }
    })

    const newItems = Object.fromEntries(items);

    return newUtensils.concat(newItems);
}

function itemsAsFragial(products){
    const newProduct = Object.entries(products[0]);
    let newUtensils;
    const items = newProduct.filter((product) => {
        if (product[0] === 'utensils') {
            const utensils = product[1].filter((inside) => {
                const insideItem = Object.entries(inside);
                const returnProduct = insideItem[0][1].type === 'fragile';
                return returnProduct;
            });
            newUtensils = utensils;
        } else {
            return product[1].type === 'fragile';
        }
    })

    const newItems = Object.fromEntries(items);

    return newUtensils.concat(newItems);
}

console.log(itemsAsFragial(products))